# Višina kleka

Podatki: [klek.csv](klek.csv)

## Opis

Meritve premera in višine na vzorcu 50 orjaških klekov (lat. Thuja plicata).

## Format

Baza podatkov s 50 meritvami dveh spremenljivk

* *premer* je numerična zvezna spremenljivka, ki predstavlja premer debla, merjen na višini
1.37 m nad tlemi (v metrih).
* *visina* je numerična zvezna spremenljivka, ki predstavlja višino drevesa (v metrih).

## Raziskovalna domneva

Med višino in premerom orjaških klekov obstaja funkcijska zveza.

## Opomba

Konstruirati linearni regresijski model med spremenljivkama visina in `log2(premer)`.