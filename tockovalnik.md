# Točkovalnik za projektno nalogo

Skupaj za nalogo lahko zberete največ **15 točk**.

## t-test za neodvisna vzorca

1. Opis podatkov in raziskovalne domneve (**1 točka**)
2. Grafični prikaz podatkov - histogram in škatla z brki (**2 točki**)
3. Opisna statistika (**1 točka**)
4. Intepretacija grafov in opisne statistike (**2 točki**)
   * znotraj skupine (1 točka)
   * med skupinama (1 točka)
5. Definiranje ničelne in alternativne domneve (**1 točka**)
6. Preverjanje predpostavk t-testa za neodvisna vzorca (**2 točki**)
   * Test normalnosti porazdelitve (1 točka)
   * Levenov test (1 točka)   
7. Intervala zaupanja za neznani povprečji dveh populacij (**2 točki**)
8. Rezultati t-testa za neodvisna vzorca in njihova interpretacija (**4 točke**)
   * testna statistika (1 točka)
   * prostostne stopnje (1 točka)
   * p-vrednost (pravilna oblika alternativne domneve) (1 točka)
   * interpretacija (1 točka) 


## Linearna regresija

1. Opis podatkov (**1 točka**)
2. Opisna statistika (**2 točki**)
   * opisna statistika netransformiranih podatkov (1 točka)
   * razlogi za transformacijo, diagnostični grafi (1 točka)
3. Razsevni diagram in vzorčni koeficient korelacije (**1 točka**)
4. Formiranje linearnega regresijskega modela  (**1 točka**) 
   * Oceni modela, enačba vzorčne reg premice (1 točka)
5. Preverjanje predpostavk linearnega modela (**4 točke**)
   * Linearnost modela (1 točka)
   * Normalnost porazdelitve naključnih napak (1 točka)
   * Konstantnost variance (1 točka)
   * Cookova razdalja
      * graf (1/2 točke)
      * analiza vpliva točk (1/2 točke)
6. Testiranje linearnosti regresijskega modela in koeficient determinacije (**2 točki**)
7. Intervala zaupanja za naklon in odsek regresijske premice (**2 točki**)
8. Interval predikcije za vrednost Y pri izbrani vrednosti X (**2 točki**)