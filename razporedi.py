#!/usr/bin/python3
# 
# Razporedi študentske projekte pri OVS po podatkih, ki so jih
# obdelovali.
#  
import os
import sys
import re
import shutil

RE_FAKS = re.compile(r'=63')
RANGE_TPL = "{:02d}_"

PROJEKTI = [
('martin/svinec'           , '63', range(00, 10)),
('martin/geom'             , '63', range(22, 31)),
('kristina/premor'  , '63', range(43, 51)),
('kristina/industr' , '63', range(62, 71)),
('alex/urpostav'    , '04', range(19, 99)),
('alex/avgume'      , '63', range(80, 85)),
('alex/jezero'      , '63', range(11, 21)),
('alex/avcena'      , '04', range( 1, 18)),
('kristina/mozgani' , '63', range(32, 42)),
('martin/klek'             , '63', range(52, 61)),
('martin/zavor'            , '63', range(72, 79)),
('kristina/forbes'  , '63', range(86, 99)),
]


if __name__ == "__main__":
    for projekt in PROJEKTI:
        ime, faks, razpon = projekt
        os.makedirs(ime, exist_ok=True)
        dirs = filter(lambda x: os.path.isdir(x), os.listdir())
        for dir in dirs:
            if RE_FAKS.search(dir) and any(dir.find(RANGE_TPL.format(n))<0 for n in razpon):
                print("Moving {} to {}".format(dir, ime))
                shutil.move(dir, ime)
